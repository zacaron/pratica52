/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author zacaron
 * @param <T>
 */
public class Equacao2Grau<T extends Number> {
    private T a;
    private T b;
    private T c;
    
    private static final String COEFICIENTE_A = "Coeficiente a não pode ser zero";
    private static final String EQUACAO_NAO_REAL = "Equação não tem solução real";
    
    @SuppressWarnings("empty-statement")
    public Equacao2Grau(T a, T b, T c) {
        if(a.intValue() == 0) {
            throw new RuntimeException(COEFICIENTE_A);
        } else {
            this.a = a;
        }
        this.b = b;
        this.c = c;
    }
    
    public double getRaiz1() {
        double aValue = getA().doubleValue();
        double bValue = getB().doubleValue();
        double cValue = getC().doubleValue();
        double x;
        if(Math.pow(bValue, 2)-4*aValue*cValue < 0){
            throw new RuntimeException(EQUACAO_NAO_REAL);
        } else {
            x = (-bValue + Math.sqrt(Math.pow(bValue, 2) - 4 * aValue * cValue)) / 2 * aValue;
        }
        
        return x;
    }
    
    public double getRaiz2() {
        double aValue = getA().doubleValue();
        double bValue = getB().doubleValue();
        double cValue = getC().doubleValue();
        double x;
        if(Math.pow(bValue, 2)-4*aValue*cValue < 0){
            throw new RuntimeException(EQUACAO_NAO_REAL);
        } else {
            x = (-bValue - Math.sqrt(Math.pow(bValue, 2) - 4 * aValue * cValue)) / 2 * aValue;
        }
        
        return x;
    }

    /**
     * @return the a
     */
    public T getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(T a) {
        if(a.intValue() == 0) {
            throw new RuntimeException(COEFICIENTE_A);
        } else {
            this.a = a;
        }
    }

    /**
     * @return the b
     */
    public T getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(T b) {
        this.b = b;
    }

    /**
     * @return the c
     */
    public T getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(T c) {
        this.c = c;
    }
}


